import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { Comment } from 'src/types';
import { usePostStore } from './post-store';

interface CommentState {
    comments: Comment[]
}

export const useCommentStore = defineStore("CommentStore", {
    state: (): CommentState => {
        return {
            comments: []
        }
    },
    actions: {
        async fetchComment(postId: number) {
            const response = await api.get(`posts/${postId}/comment`);
            this.comments = response.data.comments;
        },
        commentPosted(postId:number,comment: Comment) {
            this.comments = [...this.comments, comment]
            usePostStore().updateCommentsCount(postId, this.comments.length)
        },
        replyPosted(reply: Comment) {
            const commentIndex = this.comments.findIndex((comment) => comment.id === reply.parent_id);
            if (commentIndex !== -1) {
                //check if replies property exists on comment
                if (!this.comments[commentIndex].replies) {
                    this.comments[commentIndex].replies = [];
                }
        
                this.comments[commentIndex].replies = [...this.comments[commentIndex].replies, reply];
            }
        }
    }
})