import { defineStore } from 'pinia';
import { api } from 'src/boot/axios';
import { User, UserLoginData, UserRegisterData } from 'src/types';

export interface UserState {
	user: User
}

export const useUserStore = defineStore('UserStore', {
	state: () : UserState => {
		const userInLocalStorage = localStorage.getItem("user");
		if(userInLocalStorage) {
			return {
				user: JSON.parse(userInLocalStorage)
			}
		}

		return {
			user: {
				id: 0,
				name: '',
				avatar_url: '',
				bio: '',
				email: '',
			}
		}
	},
	actions: {
		async login(user: UserLoginData) {
			try {
				const response = await api.post("/login", user);
				const userApi = response.data.user;
				const token = response.data.token;
				localStorage.setItem("token", token);
				localStorage.setItem("user", JSON.stringify(userApi));
				this.user = userApi;
			} catch (error) {
				throw error
			}
		},
		async register(user: UserRegisterData) {
			try {
				const response = await api.post("/register", user);
				const userApi = response.data.user;
				const token = response.data.token;
				localStorage.setItem("token", token);
				localStorage.setItem("user", JSON.stringify(userApi));
				this.user = userApi;
			} catch (error) {
				throw error
			}
		},
		async logout() {
			try {
				await api.post("/logout");
				localStorage.removeItem("token");
				localStorage.removeItem("user");
				this.user = {
					id: 0,
					name: '',
					avatar_url: '',
					bio: '',
					email: '',
				}
			} catch (error) {
				throw error
			}
		}
	},
});
