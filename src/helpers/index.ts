export const urlpath = (path: string): string => {
    return process.env.VUE_APP_URL + path;
} 